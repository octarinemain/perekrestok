/* eslint-disable no-undef */
'use strict';

$(document).ready(function () {
  // Mobile menu
  function openMobMenu() {
    var btn = $('#mob-menu');
    var menu = $('#header');

    btn.on('click', function () {
      $(this).toggleClass('mob-menu_active');
      menu.toggleClass('header_active');
    });
  }
  openMobMenu();

  // PopUp
  function openPopUp() {
    $('.js-popup-button').on('click', function () {
      $('.popup').removeClass('js-popup-show');
      var popupClass = '.' + $(this).attr('data-popupShow');
      $(popupClass).addClass('js-popup-show');
      if ($(document).height() > $(window).height()) {
        var scrollTop = $('html').scrollTop() ? $('html').scrollTop() : $('body').scrollTop();
        $('html').addClass('noscroll').css('top', -scrollTop);
      }
    });
    closePopup();
  }

  // Close PopUp
  function closePopup() {
    $('.js-close-popup').on('click', function () {
      $('.popup').removeClass('js-popup-show');
      var scrollTop = parseInt($('html').css('top'));
      $('html').removeClass('noscroll');
      $('html, body').scrollTop(-scrollTop);
      return false;
    });

    $('.popup').on('click', function (e) {
      var div = $('.popup__wrap');
      var datePicker = $('#datepickers-container').length;

      if (!div.is(e.target) && div.has(e.target).length === 0 && datePicker == 0) {
        $('.popup').removeClass('js-popup-show');
        var scrollTop = parseInt($('html').css('top'));
        $('html').removeClass('noscroll');
        $('html, body').scrollTop(-scrollTop);
      }
    });
  }
  openPopUp();

  // Scroll
  $('#personal-info').find('.table-wrap__inner').scrollbar();
  $('#main').find('.winners-table__wrap').scrollbar();

  // Masked Phone
  $('input[type="tel"]').mask('+7(999)999-99-99');

  // Tabs
  function openTabs() {
    var wrap = $('#personal-info');
    var tabsContent = wrap.find('.table-wrap');
    var tabsBtn = wrap.find('.tabs li');

    // Reset
    tabsBtn.eq(0).addClass('active');
    tabsContent.eq(0).addClass('active');

    // Tabs Main Action
    wrap.find('.tabs').on('click', 'li', function () {
      var i = $(this).index();

      tabsBtn.removeClass('active');
      $(this).addClass('active');
      tabsContent.removeClass('active');
      tabsContent.eq(i).addClass('active');
    });
  }
  openTabs();

  // Select
  $('select').select2();

  // Jquery Validate
  function checkValidate() {
    var form = $('form');

    $.each(form, function () {
      $(this).validate({
        ignore: [],
        errorClass: 'error',
        validClass: 'success',
        rules: {
          name: {
            required: true,
            letters: true },

          surname: {
            required: true,
            letters: true },

          secondname: {
            required: true,
            letters: true },

          serial: {
            required: true,
            letters: true },

          email: {
            required: true,
            email: true },

          phone: {
            required: true,
            phone: true },

          city: {
            required: true,
            letters: true },

          captcha: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          password: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          home: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          apartment: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          number: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          inn: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          textarea: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          textarea1: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          calendar: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          fileupload1: {
            required: true,
            filesize: 5048576 },

          fileupload2: {
            required: true,
            filesize: 5048576 },

          fileupload3: {
            required: true,
            filesize: 5048576 } },


        messages: {
          phone: 'Некорректный номер',
          rules: {
            required: '' },

          personal: {
            required: '' },

          fileupload1: {
            required: '' },

          fileupload2: {
            required: '' },

          fileupload3: {
            required: '' } } });





      jQuery.validator.addMethod('letters', function (value, element) {
        return this.optional(element) || /^([a-zа-яё]+)$/i.test(value);
      });

      jQuery.validator.addMethod('email', function (value, element) {
        return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
      });

      jQuery.validator.addMethod('phone', function (value, element) {
        return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
      });

      jQuery.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || element.files[0].size <= param;
      });

    });
  }
  checkValidate();

  // Choose prize
  function choosePrize() {
    var wrapFrom = $('#choose-prize');
    var btn = wrapFrom.find('.btn_prize-get');
    var wrapTo = $('#items-conf');
    var imgTo = wrapTo.find('.info-img img');
    var textTo = wrapTo.find('.info-text_title');
    var pointTo = wrapTo.find('.info-item-wrap__point-num');

    btn.on('click', function () {
      var $this = $(this);
      var imgFrom = $this.
      closest('.popup__inner-items').
      find('.info-img img').
      attr('src');
      var textFrom = $this.
      parent().
      find('.info-text_title').
      text();
      var pointFrom = $this.
      closest('.info-item-wrap').
      find('.info-item-wrap__point-num').
      text();

      imgTo.attr('src', imgFrom);
      textTo.text(textFrom);
      pointTo.text(pointFrom);
    });

  }
  choosePrize();

  // Map
  var oldMarker,
  map,
  marker;

  function initMap() {
    var wrap = $('#location').find('select');
    var wrapGet = $('#get-prize-end');
    var addres = wrapGet.find('.popup__text_bold');
    var myOptions = {
      center: new google.maps.LatLng(55.74489305, -322.37937927),
      zoom: 9,
      disableDefaultUI: true,
      scrollwheel: false };


    map = new google.maps.Map(document.getElementById('map'), myOptions);

    wrap.on('change', function () {
      var $this = $(this);
      var valAddres = $this.find('option:selected').text();
      addres.text(valAddres);
      var valLoc = $this.find('option:selected').val().split(',');
      var optionFirst = $this.find('option:first-child:selected');

      if (optionFirst.length > 0) {
        initMap();
      }

      function moveToLocation(lat, lng) {
        var center = new google.maps.LatLng(lat, lng);
        map.panTo(center);
        map.setZoom(12);
      }
      moveToLocation(valLoc[0], valLoc[1]);

      marker = new google.maps.Marker({
        position: new google.maps.LatLng(valLoc[0], valLoc[1]),
        map: map,
        animation: google.maps.Animation.DROP });


      if (oldMarker != undefined) {
        oldMarker.setMap(null);
      }
      oldMarker = marker;
    });
  }

  if ($('.popup.choose-issue').length > 0) {
    initMap();
  }

  // Define mobile
  function defineMob() {
    var wrap = $('#header').find('.discription-att');
    var define = detect.parse(navigator.userAgent);
    var os = define.os.family;

    if (os == 'iOS') {
      wrap.addClass('discription-att_ios');
    } else if (os == 'Android') {
      wrap.addClass('discription-att_android');
    }
  }
  defineMob();


});